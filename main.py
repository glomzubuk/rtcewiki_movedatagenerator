#!/bin/python3
import json
import csv
import re
import sys
import os
import glob

FD_PATH = './framedata'

def character_csv_to_json(path_to_csv):
    movedata = {}
    with open(path_to_csv) as csvfile:
        movereader = csv.reader(csvfile, delimiter=',', quotechar='"')

        # Remove the csv headers
        _ = next(movereader)

        current_move = ""
        is_alt_move = False
        for move in movereader:
            # Remove empty lines
            if(move[0] == ''):
                continue

            if(move[0].startswith('-> ')):
                is_alt_move = True
            else:
                current_move = move[0]
                is_alt_move = False

            move_version = ""
            if not is_alt_move:
                move_id = move[0]

                # Find special or OD moves
                special_match = re.search(r'(.*?) \(([Ss]\d([\.vV]\d).*|OD)\)', move_id)
                if special_match != None:
                    # print(f"{move_id}")
                    move_name = special_match.group(1)
                    move_short = special_match.group(2).split('v', 1)[0]
                    
                    if 'OD' not in move_short:
                        move_short = f"{move_short}{special_match.group(3)}".replace('v','.')
                    if 'Aerial' in move_name:
                        move_short = 'j' + move_short
                else:
                    if '->' in move_id:
                        move_name = move_id.replace('(', '').replace(')', '').replace('close s.', 'cl.').replace('far s.', 'f.') \
                            .replace('->', '→')
                        if 'far' in move_id:
                            move_short = 'f.TC'
                        else:
                            move_short = 'cl.TC'
                    else:
                        move_name = move_id.replace(' Attempt', '')
                        move_short = move_id.replace('crouch ', 'cr.').replace('close s.', 'cl.').replace('far s.', 'f.') \
                        .replace('neutral ','n').replace('diagonal ', 'd').replace(' Attempt', '')
                current_move = move_id
                move_version = move_name
                movedata[current_move] = {
                    "name": move_name,
                    "short": move_short,
                    "attacks": {}
                }
            else:
                move_version = move[0].replace('-> ', '')

            attackdata = {}
            attackdata['blockzone'] = move[1].replace('H/L', 'Mid')
            attackdata['damage'] = move[2]
            attackdata['dizzy'] = move[3]
            attackdata['metergain'] = move[4]
            attackdata['cancel'] = move[5]
            attackdata['startup'] = move[6]
            attackdata['active'] = move[7]
            attackdata['recovery'] = move[8]
            attackdata['total'] = move[9]
            attackdata['advonblock'] = move[10]
            attackdata['advonhit'] = move[11]
            attackdata['blockstun'] = move[12]
            attackdata['hitstun'] = move[13]
            attackdata['cooldown'] = move[14]
            add_props = move[15].split(';')
            attack_description = ""
            for add_prop in add_props:
                add_prop = add_prop.strip()
                if ('invincible' in add_prop):
                    attackdata['invincible'] =  add_prop.split('frames ')[1]
                elif ('invulnerable' in add_prop):
                    if 'invulnerable' not in attackdata:
                        attackdata['invulnerable'] = {}
                    invul_match = re.search(r'(.*?) invulnerable (?:from|during) frames (\d+\-\d+)', add_prop)
                    attackdata['invulnerable'][invul_match.group(1)] = invul_match.group(2)
                elif (add_prop.startswith('airborne')):
                    attackdata['airborne'] = add_prop.split('frames ')[1]
                elif ('whiffs' in add_prop):
                    whiff_match = re.search(r'(?:against|over) ([\w ]+) opponent', add_prop)
                    attackdata['whiffs'] = whiff_match.group(1)
                else:
                    attack_description += add_prop + "; "
            attackdata['description'] = re.sub(r'; $', '', attack_description)
            movedata[current_move]['attacks'][move_version] = attackdata
    return movedata



def gen_attackdata_wiki(attackdata_json, version = None, header = True, indent = 1):
    intent_string = ""
    for _ in range(indent):
        intent_string += "  "

    attackdata_wiki = intent_string + "{{AttackData-RT\n"
    if version != None:
        attackdata_wiki += intent_string + f"|version={version}\n"
    attackdata_wiki += intent_string + f"|damage={attackdata_json['damage']}\n"
    attackdata_wiki += intent_string + f"|stun={attackdata_json['dizzy']}\n"
    attackdata_wiki += intent_string + f"|guard={attackdata_json['blockzone']}"
    if 'whiffs' in attackdata_json:
        attackdata_wiki += f" (Whiffs {attackdata_json['whiffs']})\n"
    else:
        attackdata_wiki += '\n'
    attackdata_wiki += intent_string + f"|cancel={attackdata_json['cancel']}\n"
    attackdata_wiki += intent_string + f"|startup={attackdata_json['startup']}\n"
    attackdata_wiki += intent_string + f"|active={attackdata_json['active']}\n"
    attackdata_wiki += intent_string + f"|recovery={attackdata_json['recovery']}\n"
    attackdata_wiki += intent_string + f"|total={attackdata_json['total']}\n"
    attackdata_wiki += intent_string + f"|advHit={attackdata_json['advonhit']}\n"
    attackdata_wiki += intent_string + f"|advBlock={attackdata_json['advonblock']}\n"
    attackdata_wiki += intent_string + f"|description={attackdata_json['description']}\n"
    attackdata_wiki += intent_string + "}}\n"
    return attackdata_wiki

def gen_movedata_wiki(character_name, movedata_json):
    move_name_wiki = movedata_json['name']
    move_id_wiki = movedata_json['short']
    strength = get_move_strength(move_id_wiki)
    # print(f"{move_id_wiki} === {strength}")
    
    movedata_wiki  = f"===<big>{{{{clr|{strength}|{move_id_wiki}}}}}</big>===\n"
    movedata_wiki += f"<section begin={move_id_wiki}/>\n"
    movedata_wiki += "{{MoveData\n"
    movedata_wiki += f"|name={move_name_wiki}\n"
    movedata_wiki += f"|image={character_name} {move_id_wiki}.png\n"
    movedata_wiki += f"|caption=\n"
    movedata_wiki += f"|data=\n"
    if(len(movedata_json['attacks']) > 1):
        for ad_index, ad_key in enumerate(movedata_json['attacks']) :
            movedata_wiki += gen_attackdata_wiki(movedata_json['attacks'][ad_key], ad_key, ad_index == 0)
    else:
        movedata_wiki += gen_attackdata_wiki(list(movedata_json['attacks'].values())[0])
    movedata_wiki += "}}\n"
    movedata_wiki += f"<section end={move_id_wiki}/>\n"

    return movedata_wiki

def gen_chardata_wiki(character_name, chardata_json):
    chardata_wiki = "<section begin=Movelist/>\n"
    for movedata_json in chardata_json.values():
        chardata_wiki += gen_movedata_wiki(character_name, movedata_json)
    chardata_wiki += "<section end=Movelist/>\n"
    chardata_wiki += '<templatestyles src="clr/styles.css"/>\n'
    return chardata_wiki + "[[Category:Rising Thunder: Community Edition]]"


def get_char_source_csv(framedata_path):
    return glob.glob(framedata_path + '/csv/*.csv')

def get_move_strength(move_id):
    switcher = {
        "L": 2,
        "M": 5,
        "H": 4,
        "C": 4
    }
    return str(switcher.get(move_id[-1], get_special_type(move_id)))

def get_special_type(move_id):
    switcher = {
        "1": 6,
        "2": 3,
        "3": 8
    }
    return switcher.get(move_id.split(".", 1)[0][-1], "0")

def write_wiki_script(framedata_path, character_name, wiki_script):
    wiki_script_dir = framedata_path + "/wiki_script"
    if not os.path.isdir(wiki_script_dir):
        os.mkdir(wiki_script_dir)
    with open(wiki_script_dir + f"/{character_name}.txt" , 'w', encoding="utf-8") as ws_file:
        ws_file.write(wiki_script)

def end():
    sheeps = r'''           __  _
       .-.'  `; `-._  __  _
      (_,         .-:'  `; `-._
    ,'o"(        (_,           )
   (__,-'      ,'o"(            )>
      (       (__,-'            )
       `-'._.--._(             )
          |||  |||`-'._.--._.-'
                     |||  |||'''
    print(sheeps)


if __name__ == '__main__':
    csv_file_paths = get_char_source_csv(FD_PATH)
    for csv_file_path in csv_file_paths:
        char_name, _ = os.path.splitext(os.path.basename(csv_file_path))
        print(f"Parsing csv for {char_name}.")
        character_json = character_csv_to_json(csv_file_path)
        print(f"Generating wiki script for {char_name}.")
        write_wiki_script(FD_PATH, char_name, gen_chardata_wiki(char_name, character_json))
    #end()
